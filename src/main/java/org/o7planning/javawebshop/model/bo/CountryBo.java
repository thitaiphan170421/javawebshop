package org.o7planning.javawebshop.model.bo;

import java.util.ArrayList;

import org.o7planning.javawebshop.model.bean.CountryBean;
import org.o7planning.javawebshop.model.dao.CountryDao;


public class CountryBo {
	private CountryDao countryDao = new CountryDao();

	public ArrayList<CountryBean> gets() throws Exception {
		return countryDao.gets();
	}
}
